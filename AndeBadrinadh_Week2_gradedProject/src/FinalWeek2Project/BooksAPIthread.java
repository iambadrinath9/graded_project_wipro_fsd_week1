package FinalWeek2Project;
import java.util.*;
public class BooksAPIthread implements Runnable {
	//ArrayList is used to store the User objects
	private static ArrayList<User> userlist = new ArrayList<>();
	@Override
	//using  threads and Overriding the run method
	public void run() {
		Scanner scan = new Scanner(System.in);
	
		User u1 = new User("Adam Sandler",123,"AdamSandler@gmail.com","AdamSandler123");// creating user1
		ArrayList<Book> newbooks=u1.getNewbooks(new Book("Ulysses","James Joyce","Ulysses by James Joyce",101));//creating list of new books of User  object 1
		ArrayList<Book> FavouriteBooks=u1.getFavouritebooks(new Book("Don Quixote", "Miguel de Cervantes", "Don Quixote by Miguel de Cervantes", 102),
				new Book("One Hundred Years of Solitude","Gabriel Garcia Marquez","One Hundred Years\r"
						+ "of Solitude by Gabriel Garcia Marquez",103));//creating list of Favouite books of User  object 1
		ArrayList<Book> CompletedBooks= u1.getCompletedbooks(new Book("Hamlet","William Shakespeare","Hamlet by William Shakespeare", 104));//creating list of Completed books of User  object 1
		u1.getMap().put("New Books", newbooks);// inserting user object 1 newbooks in map
		u1.getMap().put("Favourite Books", FavouriteBooks);// inserting user object 1 Favourite in map
		u1.getMap().put("Completed Books", CompletedBooks);// inserting user object 1 Completed in map
		userlist.add(u1);// adding User object into Userlist
		
		//user 2 details which are given in the sample test cases
		
		User u2 = new User("Tom Hanks",124,"TomHanks@gmail.com","TomHanks123");
		ArrayList<Book> u2newBook= u2.getNewbooks(	new Book("One Hundred Years of Solitude","Gabriel Garcia Marquez","One Hundred Years\r"
				+ "of Solitude by Gabriel Garcia Marquez",103));
		ArrayList<Book> u2favouritebook = u2.getFavouritebooks(new Book("Ulysses","James Joyce","Ulysses by James Joyce",101),new Book("Hamlet","William Shakespeare","Hamlet by William Shakespeare", 104));
		ArrayList<Book> u2Completedbook = u2.getCompletedbooks(new Book("In Search of Lost Time", "Marcel Proust", "In Search of Lost Time by Marcel Proust",105 ));
		u2.getMap().put("New Books", u2newBook);
		u2.getMap().put("Favourite Books", u2favouritebook);
		u2.getMap().put("Completed Books", u2Completedbook);
		
		userlist.add(u2);
		
		//user 3 details which are given in the sample test cases
		
		User u3 = new User("Tom Cruise", 125, "TomCruise@gmail.com", "TomCruise123");//creating user 3
		ArrayList<Book> u3newBook=u3.getNewbooks(new Book("Don Quixote", "Miguel de Cervantes", "Don Quixote by Miguel de Cervantes", 102));
		ArrayList<Book> u3FavouriteBook = u3.getFavouritebooks(new Book("One Hundred Years of Solitude","Gabriel Garcia Marquez","One Hundred Years\r"
				+ "of Solitude by Gabriel Garcia Marquez",103));
		ArrayList<Book> u3Completedbook= u3.getCompletedbooks(new Book("Hamlet","William Shakespeare","Hamlet by William Shakespeare", 104),new Book("In Search of Lost Time", "Marcel Proust", "In Search of Lost Time by Marcel Proust",105 ));
		u3.getMap().put("New Books", u3newBook);
		u3.getMap().put("Favourite Books", u3FavouriteBook);
		u3.getMap().put("Completed Books", u3Completedbook);
		userlist.add(u3);
		
		//user 4   which are given in the sample test cases
		User u4 = new User("Angelina Jolie", 126, "AngelinaJolie@gmail.com", "AngelinaJolie.123");
		ArrayList<Book> u4newBooks= u4.getNewbooks(new Book("Ulysses","James Joyce","Ulysses by James Joyce",101),new Book("In Search of Lost Time", "Marcel Proust", "In Search of Lost Time by Marcel Proust",105 ));
		ArrayList<Book> u4Favouritebook = u4.getFavouritebooks(new Book("Don Quixote", "Miguel de Cervantes", "Don Quixote by Miguel de Cervantes", 102));
		ArrayList<Book> u4CompletedBook =u4.getCompletedbooks(new Book("Don Quixote", "Miguel de Cervantes", "Don Quixote by Miguel de Cervantes", 102),new Book("Hamlet","William Shakespeare","Hamlet by William Shakespeare", 104));
		u4.getMap().put("New Books", u4newBooks);
		u4.getMap().put("Favourite Books", u4Favouritebook);
		u4.getMap().put("Completed Books", u4CompletedBook);
		userlist.add(u4);
		
		// user 5 details  which are given in the sample test cases
		User u5 = new User("Scarlett Johansson", 127, "ScarlettJohansson@gmail.com", "ScarlettJohansson123");
		ArrayList<Book> u5newBooks= u5.getNewbooks(new Book("In Search of Lost Time", "Marcel Proust", "In Search of Lost Time by Marcel Proust",105 ));
		ArrayList<Book> u5FavouriteBooks=u5.getFavouritebooks(new Book("Ulysses","James Joyce","Ulysses by James Joyce",101));
		ArrayList<Book> u5CompletedBooks = u5.getCompletedbooks(new Book("Hamlet","William Shakespeare","Hamlet by William Shakespeare", 104));
		u5.getMap().put("New Books", u5newBooks);
		u5.getMap().put("Favourite Books", u5FavouriteBooks);
		u5.getMap().put("Completed Books", u5CompletedBooks);
		userlist.add(u5);
		
		boolean quit=false;
		boolean ifcon=false;
		while(!quit) {
			System.out.println("Please enter your username:");
			String name=scan.nextLine();
			if(getUserObject(name)!=null) {
			for(User i: userlist) {
				if(i.getUserName().equals(name) || i.getUserName().toLowerCase().equals(name)) {
					ifcon=true;
					System.out.println("Welcome "+name+"!");
					display();
					int choice =Integer.parseInt(scan.nextLine());
					
					if(choice==1) {
						/*
						 * implemention of code Print the Booknames*/
						User u11 = getUserObject(i.getUserName());
						for(String key : u11.getMap().keySet()) {
							System.out.println(key+":");	
							//System.out.println(key +"==="+ u1.getMap().get(key).get(0).getBookName());					
							for (int i1=0;i1<u11.getMap().get(key).size();i1++) {
							System.out.println( u11.getMap().get(key).get(i1).getBookName());
				
							}
						}
						System.out.println("Do you want to continue?");
						String userin =scan.nextLine();
						if(userin.equals("no")||userin.equals("NO")) {
							quit=true;
						}
						else if (userin.equals("yes")||userin.equals("YES")) {
							quit=false;
						}
						else {
							try {
								throw new WrongInputException("(yes/no)Given Wrong input");
							}catch (Exception e) {
								System.out.println(e);
							}finally {
								quit=true;
							}
						}
						
					}
					else if(choice == 2) {
						//implementation of the code to check whether the book is available 
						System.out.println("Please enter your book id:");
						int id = Integer.parseInt(scan.nextLine());
						boolean found = false;
						User u12 = getUserObject(i.getUserName());
						for(String key: u12.getMap().keySet()) {
							for(int i1=0;i1<u12.getMap().get(key).size();i1++) {
								//System.out.println(u12.getMap().get(key).get(i1).getBookId());
								if(u12.getMap().get(key).get(i1).getBookId().equals(id)) {
									found=true;
								}
							}
						}
						if(found==true) {
							System.out.println("Available");
						}
						else {
							try {
								throw new BookUnavilableException();
							}
							catch (Exception e) {
								System.out.println("Unavilable");
							}
//							System.out.println("Unavailable"
//									+ "");
						}
						System.out.println("Do you want to continue?");
						String userin =scan.nextLine();
						if(userin.equals("no")||userin.equals("NO")) {
							quit=true;
						}
						else if (userin.equals("yes")||userin.equals("YES")) {
							quit=false;
						}
						else {
							try {
								throw new WrongInputException("(yes/no)Given Wrong input");
							}catch (Exception e) {
								System.out.println(e);
							}finally {
								quit=true;
							}
						}
					}
					else if(choice==3) {
						//implementation of code to print the details of the book name entered by the user
						System.out.println("Please enter book name:");
						String bookname =scan.nextLine();
						boolean check=false;
						User u13 = getUserObject(i.getUserName());
						for(String key :u13.getMap().keySet()) {
							for(int i1=0;i1<u13.getMap().get(key).size();i1++) {
								if(u13.getMap().get(key).get(i1).getBookName().equals(bookname) ) {
									System.out.println("Author Name: "+u13.getMap().get(key).get(i1).getAuthorname());

									System.out.println("Description: "+u13.getMap().get(key).get(i1).getDescription());
									check=true;
								}
								if(u13.getMap().get(key).get(i1).getBookName().toLowerCase().equals(bookname) ) {
									System.out.println("Author Name: "+u13.getMap().get(key).get(i1).getAuthorname());
									System.out.println("Description: "+u13.getMap().get(key).get(i1).getDescription());
									check=true;
								}
							}
						}
						if(check==false) {
							try {
								throw new BookUnavilableException();
							}catch (Exception e) {
								System.out.println("Book not found!");
							}
							//System.out.println("Book not found!");
						}
						System.out.println("Do you want to continue?");
						String userin =scan.nextLine();
						if(userin.equals("no")||userin.equals("NO")) {
							quit=true;
						}
						else if (userin.equals("yes") || userin.equals("YES")) {
							quit=false;
						}
						else {
							try {
								throw new WrongInputException("(yes/no)Given Wrong input");
							}catch (Exception e) {
								System.out.println(e);
							}finally {
								quit=true;
							}
						}
						
					}
					else {
						try {
							throw new WrongInputException();
						}catch (Exception e) {
							System.out.println("Enterred the Wrong input");
						}
						
					quit=true;	
					break;
						
					}
					
					
				}
				
				}//forloop-end
			}else {
				try {
					throw new UsernameException();
				}catch (Exception e) {
					System.out.println("Sorry! You are not an authorized user");// TODO: handle exception
				}
				quit=true;
				break;

			}
			
			
			
				
		}//while-end
		
	
	}
	//this method returns the User Object with the name passed to method
	public static User getUserObject(String name) {
		for (User i:userlist) {
			if(i.getUserName().equals(name) || i.getUserName().toLowerCase().equals(name)) {
				return i;
			}
		}
		return null;
	}
	//this method is used to display the menu to the user
	public static void display() {
		System.out.println("************** MENU ***********\n"+
					"1.Print your books(new,favourite,completed)\n"+
					"2.Find the Book by Book Id\n"+
					"3.Print the details of a book\n"+
                	"Please enter your choice :");
	}
	

}

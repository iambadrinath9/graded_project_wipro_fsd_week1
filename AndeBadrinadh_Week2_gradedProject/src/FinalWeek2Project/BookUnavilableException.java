package FinalWeek2Project;

public class BookUnavilableException extends Exception{

	/**
	 *this method is used to throw the exception when wrong book name is passed or Entered  
	 */
	private static final long serialVersionUID = 1L;

	public BookUnavilableException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public BookUnavilableException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public BookUnavilableException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public BookUnavilableException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public BookUnavilableException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}
	
	

}

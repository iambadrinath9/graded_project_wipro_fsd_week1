package FinalWeek2Project;

public class UsernameException extends Exception{

	/**
	 * this method is used to throw the Exception 
	 * when the user input in wrong
	 */
	private static final long serialVersionUID = 1L;

	public UsernameException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public UsernameException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		
	}

	public UsernameException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public UsernameException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public UsernameException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}
	


}

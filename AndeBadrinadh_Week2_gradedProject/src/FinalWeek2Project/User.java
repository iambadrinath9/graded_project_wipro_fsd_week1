package FinalWeek2Project;
import java.util.*;
//pojo class
public class User {
	//user class attributes
	private String userName;
	private int userId;
	private String emailId;
	private String password;
	private  Map<String,ArrayList<Book>> map ;
	//user class parameterized constructor
	public User(String userName, int userId, String emailId, String password) {
		super();
		this.userName = userName;
		this.userId = userId;
		this.emailId = emailId;
		this.password = password;
		this.map=new LinkedHashMap<>();
		
	}
	//setters and getters for the User class
	public String getUserName() {
		return userName;
	}
	public int getUserId() {
		return userId;
	}
	public String getEmailId() {
		return emailId;
	}
	public String getPassword() {
		return password;
	}
	public Map<String, ArrayList<Book>> getMap() {
		return map;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	//this method gives the ArrayList of newBooks
	public ArrayList<Book> getNewbooks(Book...n){
		ArrayList<Book> newBook = new ArrayList<Book>();
		for(Book i: n) {
			newBook.add(i);
		}
		return newBook;
	}
	//this method gives the ArrayList of FavouriteBooks
	public ArrayList<Book> getFavouritebooks(Book...n){
		ArrayList<Book> newBook = new ArrayList<Book>();
		for(Book i: n) {
			newBook.add(i);
		}
		return newBook;
	}
	//this method gives the ArrayList of CompletedBooks
	public ArrayList<Book> getCompletedbooks(Book...n){
		ArrayList<Book> newBook = new ArrayList<Book>();
		for(Book i: n) {
			newBook.add(i);
		}
		return newBook;
	}
	
	/*
	 * In the above methods variable length arguments are passed*/

}

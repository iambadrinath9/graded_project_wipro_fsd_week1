package FinalWeek2Project;

public class MagicOfBooks {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		/*Description:
		 this is the main class of API
		 created a Thread class and Functionality of menu is  implemented here
		  Books Ids are 101,102,103,104,105
		  bookid    Book names :
		  101   	Ulysses  
		  102 		Don Quixote
		  103 		One Hundred Years of Solitude
		  104 		Hamlet
		  105  		In Search of Lost Time
		  */
		BooksAPIthread m= new BooksAPIthread();
		Thread t = new Thread(m);
		t.start();

	}

}

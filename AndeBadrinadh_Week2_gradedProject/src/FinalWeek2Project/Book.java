package FinalWeek2Project;

public class Book {
	    //book class attributes

		private String bookName;
		private String Authorname;
		private String description;
		private int bookId;
		//parameterized constructor
		public Book(String bookName, String authorname, String description, int bookId) {
			super();
			this.bookName = bookName;
			Authorname = authorname;
			this.description = description;
			this.bookId = bookId;
		}
		//setters and getters for the book class
		public String getBookName() {
			return bookName;
		}
		public String getAuthorname() {
			return Authorname;
		}
		public String getDescription() {
			return description;
		}
		public Integer getBookId() {
			return bookId;
		}
		public void setBookName(String bookName) {
			this.bookName = bookName;
		}
		public void setAuthorname(String authorname) {
			Authorname = authorname;
		}
		public void setDescription(String description) {
			this.description = description;
		}
		public void setBookId(int bookId) {
			this.bookId = bookId;
		}
		@Override
		public String toString() {
			return "Book [bookName=" + bookName + ", Authorname=" + Authorname + ", description=" + description
					+ ", bookId=" + bookId + "]";
		}
		
		/*
		 * this Book class object is used in the User class
		 */

	

}
